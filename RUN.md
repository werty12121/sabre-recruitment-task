### How to run this program

1) First you need to clone this repo by typing: 
```
git clone https://werty12121@bitbucket.org/werty12121/sabre-recruitment-task.git
```
    
2) After that, you need to compile Java code:
```
javac sabre_recruitment_task/src/com/werty12121/sabre_recruitment_task/Main.java
```
    
3) Next, you need to go to this specific folder in terminal and finally run the code:
```
cd sabre_recruitment_task/src
java com.werty12121.sabre_recruitment_task.Main
```