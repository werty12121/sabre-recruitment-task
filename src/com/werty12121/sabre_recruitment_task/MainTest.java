package com.werty12121.sabre_recruitment_task;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MainTest {
    @Test
    void cleaningShouldThrowErrorIfTextIsNull() {
        Main testObject = new Main();
        testObject.text = null;
        assertThrows(NullPointerException.class, testObject::cleanInputText);
    }

    @Test
    void cleaningShouldLowercaseText() {
        Main testObject = new Main();
        testObject.text = "TeSt ALA ma KOTA";
        testObject.cleanInputText();
        assertEquals("test ala ma kota", testObject.text);
    }

    @Test
    void cleaningShouldRemovePunctuationsFromText() {
        Main testObject = new Main();
        testObject.text = "!test!, !@#$%^&*()(_+[];',./<>?:{}ala, ma kota;?";
        testObject.cleanInputText();
        assertEquals("test ala ma kota", testObject.text);
    }

    @Test
    void cleaningShouldRemoveDoubleSpacesFromText() {
        Main testObject = new Main();
        testObject.text = "test  ala        ma      kota";
        testObject.cleanInputText();
        assertEquals("test ala ma kota", testObject.text);
    }

    @Test
    void cleaningShouldTrimText() {
        Main testObject = new Main();
        testObject.text = "  test ala ma kota     ";
        testObject.cleanInputText();
        assertEquals("test ala ma kota", testObject.text);
    }

    @Test
    void indexingShouldThrowErrorIfIndexedTextIsNull() {
        Main testObject = new Main();
        testObject.indexedText = null;
        assertThrows(NullPointerException.class, testObject::indexWordsByLetter);
    }

    @Test
    void indexingShouldIgnoreDuplicates() {
        Main testObject = new Main();
        testObject.text = "a a a";
        testObject.indexWordsByLetter();
        HashMap<Character, ArrayList<String>> temp = new HashMap<>();
        ArrayList<String> tempArrayList = new ArrayList<>();
        tempArrayList.add("a");
        temp.put('a', tempArrayList);
        assertEquals(temp, testObject.indexedText);
    }

    @Test
    void indexingShouldReturnEmptyIfTextIsEmpty() {
        Main testObject = new Main();
        testObject.text = "";
        testObject.indexWordsByLetter();
        HashMap<Character, ArrayList<String>> temp = new HashMap<>();
        assertEquals(temp, testObject.indexedText);
    }
    @Test
    void indexingShouldNotStopIndexingSpecificWordWhenCharacterIsDuplicated() {
        Main testObject = new Main();
        testObject.run("aab aab aabc");
        HashMap<Character, ArrayList<String>> temp = new HashMap<>();
        ArrayList<String> tempArrayList = new ArrayList<>();
        tempArrayList.add("aab");
        tempArrayList.add("aabc");
        temp.put('a', tempArrayList);
        temp.put('b', tempArrayList);
        tempArrayList = new ArrayList<>();
        tempArrayList.add("aabc");
        temp.put('c', tempArrayList);
        assertEquals(temp, testObject.indexedText);
    }
}