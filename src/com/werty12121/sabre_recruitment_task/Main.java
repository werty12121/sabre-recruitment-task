package com.werty12121.sabre_recruitment_task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    String text;
    HashMap<Character, ArrayList<String>> indexedText;

    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    void cleanInputText() {
        if (this.text == null) {
            throw new NullPointerException("Text cannot be NULL !!!");
        }
        this.text = this.text.toLowerCase();
        this.text = this.text.replaceAll("[^a-zążźćęółń ]", " ");
        this.text = this.text.replaceAll("\\s{2,}", " ");
        this.text = this.text.trim();
    }

    private String getInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private void sortAndShow() {
        if (this.text == null) {
            throw new NullPointerException("indexedText cannot be NULL !!! you need to run indexWordsByLetter function first.");
        }
        ArrayList<Character> keys = new ArrayList<>(this.indexedText.keySet());
        Collections.sort(keys);
        for (Character key : keys) {
            ArrayList<String> temp = this.indexedText.get(key);
            Collections.sort(temp);
            System.out.println(key + ": " + temp.toString().replaceAll("[\\[\\]]", ""));
        }
    }

    public void run() {
        this.run(this.getInput());
    }

    public void run(String inputText) {
        this.text = inputText;
        this.cleanInputText();
        this.indexWordsByLetter();
        this.sortAndShow();
    }

    void indexWordsByLetter() {
        if (this.text == null) {
            throw new NullPointerException("Text cannot be NULL !!!");
        }
        this.indexedText = new HashMap<>();
        for (String word : this.text.split(" ")) {
            boolean firstCheck = true;
            for (char character : word.toCharArray()) {
                ArrayList<String> element = this.indexedText.get(character);
                if (element == null) {
                    element = new ArrayList<>();
                } else if (element.contains(word)) {
                    if (!firstCheck) {
                        continue;
                    } else {
                        break;
                    }
                }
                firstCheck = false;
                element.add(word);
                this.indexedText.put(character, element);
            }
        }
    }
}
